export class LoggingService {
  activeCounter = 0;
  inactiveCounter = 0;

  onActivate() {
    console.log('activating: ', ++this.activeCounter);
  }

  onInactivate() {
    console.log('deActivating: ', ++this.inactiveCounter);
  }
}
